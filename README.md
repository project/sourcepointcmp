<img src="/images/logo.png" width=25%>

# Sourcepoint CMP Drupal Module

[![Build Status](https://travis-ci.org/username/repository.svg?branch=main)](https://travis-ci.org/username/repository)
[![Coverage Status](https://coveralls.io/repos/github/username/repository/badge.svg?branch=main)](https://coveralls.io/github/username/repository?branch=main)
[![License: GPL-2.0](https://img.shields.io/badge/license-GPL--2.0-blue.svg)](https://opensource.org/licenses/GPL-2.0)

The [Sourcepoint CMP Dupal Module](https://www.drupal.org/project/sourcepointcmp) is a Drupal module that implements Sourcepoint Consent Management Platform (CMP) into your Drupal CMS. Sourcepoint Consent Management Platform (CMP) is a robust solution designed to help organizations achieve compliance with global data protection regulations such as GDPR and CCPA. It allows publishers and marketers to gather, manage, and document user consent for data processing activities. Sourcepoint CMP ensures transparent communication with users, enabling them to make informed choices about their privacy preferences. The platform supports seamless integration with various digital properties, ensuring consent signals are consistently handled across multiple applications and services.

- [Requirements](#requirements)
- [Install Sourcepoint CMP Drupal Module](#install-sourcepoint-cmp-drupal-module)
- [Configure Sourcepoint CMP Drupal Module](#configure-sourcepoint-cmp-drupal-module)

> The following sections assume that you have configured the necessary vendor lists, messages and other campaign entities for your web property within the Sourcepoint portal. [Click here](https://docs.sourcepoint.com/hc/en-us) to visit the Sourcepoint help center for more information.

## Requirements

- Drupal 8 or higher
- Compatible with Sourcepoint web properties using multi-campaign (4.x.x)

## Install Sourcepoint CMP Drupal Module

To add and install the Sourcepoint CMP Drupal Module, perform the following:

1. Add the module to your Drupal project. Drupal supports multiple ways to add a module - [click here](https://www.drupal.org/docs/extending-drupal/installing-modules#s-step-1-add-the-module) to discover the best method for you organization.
2. Install the module via the Drupal interface (by selecting the module checkbox on the **Extend** page and clicking **Install**) or via the command line.
3. Set user permissions for module by expanding the description of the module and clicking **Permissions**.

## Configure Sourcepoint CMP Drupal Module

> Ensure that your organization has configured the necessary vendor list(s) and campaign entities for your web property within the Sourcepoint portal before saving your configuration. [Click here](https://docs.sourcepoint.com/hc/en-us) to visit the Sourcepoint help center for more information.

Once the Sourcepoint CMP Dupral Module is added and installed on your project, navigate to the module on the **Extend** page. Expand the description of the module and click **Configure**.

From the subsequent page your organization can enter its account information and campaign message types. Please refer to the table below for each available field:

| Field          | Description                                                                                                                                                                                                                                                                                                                                                                                                                               |
| -------------- | ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| enable CMP     | Indicates whether the CMP messaging experience being configured in the plugin should be added to your webpage.                                                                                                                                                                                                                                                                                                                            |
| Account Number | The account ID value associates the property with your organization's Sourcepoint account. Your organization's account ID can be retrieved by contacting your Sourcepoint Account Manager or via the My Account page in your Sourcepoint account.                                                                                                                                                                                         |
| propertyHref   | Maps the implementation to a specific URL as set up in the Sourcepoint account dashboard. [Click here](https://docs.sourcepoint.com/hc/en-us/articles/8938401981843-Best-practices-propertyHref) for more information and best practices.                                                                                                                                                                                                 |
| propertyID     | Maps the message to a specific property (website, app, OTT) as set up in Sourcepoint account dashboard.                                                                                                                                                                                                                                                                                                                                   |
| baseEndpoint   | The baseEndpoint is a single server endpoint that serves the messaging experience. By default the baseEndpoint is https://cdn.privacy-mgmt.com however it can be changed to a [CNAME first-party subdomain](https://docs.sourcepoint.com/hc/en-us/articles/4405397441043-Configure-subdomain-with-CNAME-DNS-record) to persist first-party cookies on Safari web browser. Leave field blank if your organization is **not** using a CNAME |
| enableGDPR     | Select if your property has a GDPR TCF message campaign configured for the web property.                                                                                                                                                                                                                                                                                                                                                  |
| enableUSNAT    | Select if your property has a U.S. Multi-State Privacy message campaign configured for the web property.                                                                                                                                                                                                                                                                                                                                  |
| enable CCPA    | Select if your property has a U.S. Privacy (Legacy) message campaign configured for the web property.                                                                                                                                                                                                                                                                                                                                     |

> Your organization will need to set a value for **either** `propertyHref` or `propertyID` but **not both**. [Click here](https://docs.sourcepoint.com/hc/en-us/articles/8938401981843-Best-practices-propertyHref#h_01GB81HYASFG9GJ4Q3GR0VSQK2) for more information.

Click **Save configuration** when finished. If **enable CMP** is enabled, Sourcepoint's tags will be added to your webpage and your consent message experience will be delivered in accordance with your scenario configuration.
