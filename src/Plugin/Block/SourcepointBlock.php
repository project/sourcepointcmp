<?php

namespace Drupal\sourcepoint_cmp\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Block\Attribute\Block;
use Drupal\Core\StringTranslation\TranslatableMarkup;

/**
 * Provides a 'Hello' Block.
 */
#[Block(
  id: "sourcepoint_cmp",
  admin_label: new TranslatableMarkup("Sourcepoint CMP Module"),
  category: new TranslatableMarkup("Privacy Apps")
)]
class SourcepointBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    return [
      '#markup' => $this->t('Sourcepoint CMP Module'),
    ];
  }

}
?>