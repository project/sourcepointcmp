<?php

namespace Drupal\sourcepoint_cmp\Form;

use Drupal\sourcepoint_cmp\SourcepointCmpManager;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Extension\ModuleExtensionList;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Form for the Consent Manager settings.
 */
class SourcepointCmpSettingsForm extends ConfigFormBase {

  /**
   * @var \Drupal\Core\Extension\ModuleExtensionList
   */

  protected $moduleExtensionList;

  /**
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\Core\Extension\ModuleExtensionList $module_extension_list
   * 
   */

  public function __construct(ConfigFactoryInterface $config_factory, ModuleExtensionList $module_extension_list) {
    parent::__construct($config_factory);
    $this->moduleExtensionList = $module_extension_list;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('extension.list.module'),
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'sourcepoint_cmp.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'sourcepoint_cmp_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('sourcepoint_cmp.settings');
    $path = '/' . $this->moduleExtensionList->getPath('sourcepoint_cmp');

    $form['cmp'] = array(
        '#type' => 'checkbox',
        '#title' => $this->t('enable CMP'),
        '#default_value' => $config->get('cmp'),
    );

    $form['account_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Account ID'),
      '#description' => $this->t('desc for AccountID'),
      '#default_value' => $config->get('account_id'),
      '#required' => TRUE,
    ];

    $form['property_href'] = [
      '#type' => 'textfield',
      '#title' => $this->t('PropertyHref'),
      '#description' => $this->t('desc for PropertyHref'),
      '#default_value' => $config->get('property_href'),
    ];

    $form['property_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('PropertyID'),
      '#description' => $this->t('desc for property_id'),
      '#default_value' => $config->get('property_id'),
    ];

    $form['base_endpoint'] = [
      '#type' => 'textfield',
      '#title' => $this->t('BaseEndpoint'),
      '#description' => $this->t('desc for base_endpoint'),
      '#default_value' => $config->get('base_endpoint'),
    ];

    $form['gdpr'] = array(
        '#type' => 'checkbox',
        '#title' => $this->t('enable GDPR'),
        '#default_value' => $config->get('gdpr'),
    );

    $form['usnat'] = array(
        '#type' => 'checkbox',
        '#title' => $this->t('enable USNAT'),
        '#default_value' => $config->get('usnat'),
    );

    $form['ccpa'] = array(
        '#type' => 'checkbox',
        '#title' => $this->t('enable CCPA (legacy)'),
        '#default_value' => $config->get('ccpa'),
    );

   /* $form['onconsentready'] = array(
      '#type' => 'text_format',
      '#title' => 'Body',
      '#format' => 'source',
      '#default_value' => '<p>The quick brown fox jumped over the lazy dog.</p>',
    );*/

    return parent::buildForm($form, $form_state);
  }



  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $host = $form_state->getValue('host');
    if ($host && !filter_var($host, FILTER_VALIDATE_DOMAIN, FILTER_FLAG_HOSTNAME)) {
      $form_state->setErrorByName('host', $this->t('Wrong host value.'));
    }

    $cdn = $form_state->getValue('cdn');
    if ($cdn && !filter_var($cdn, FILTER_VALIDATE_DOMAIN, FILTER_FLAG_HOSTNAME)) {
      $form_state->setErrorByName('cdn', $this->t('Wrong CDN value.'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $this->config('sourcepoint_cmp.settings')
      ->set('cmp', trim($form_state->getValue('cmp')))
      ->set('account_id', trim($form_state->getValue('account_id')))
      ->set('property_href', trim(str_replace(array("/", "http:", "https:"), "", $form_state->getValue('property_href'))))
      ->set('gdpr', trim($form_state->getValue('gdpr')))
      ->set('base_endpoint', trim(str_replace(array("/", "http:", "https:"), "", $form_state->getValue('base_endpoint'))))
      ->set('usnat', trim($form_state->getValue('usnat')))
      ->set('ccpa', trim($form_state->getValue('ccpa')))
      ->save();




    
    Cache::invalidateTags(['sourcepoint_cmp']);
  }

}
